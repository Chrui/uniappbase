import Vue from 'vue'
import App from './App'


Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
	...App
})


//网络请求封装
import  http from './util/http.js'
Vue.prototype.$http = http
//网络请求封装结束

// #ifdef APP-PLUS
import socket from './js_sdk/plus-websocket/index.js'
Object.assign(uni, socket)
// #endif
app.$mount()
